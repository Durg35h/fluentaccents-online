import sys
import math
import json

factor = sys.argv[1]

utt_id = ""

def norm(raw_score):
	return math.exp(float(raw_score)/float(factor))*100

def norm_in_range(value,min_,max_):
	return (value-min_)/(max_-min_)

data_dir=sys.argv[2]

phone_ali_file = data_dir+"/force_align/out_phone_ctm"
text_ali_file = data_dir+"/force_align_lats/out_ctm"

p_start = []
p_end = []
phones = []

w_start = []
w_end = []
words = []

sil_count = 0
min_range = 0.0
max_range = 5.0

with open(text_ali_file,"r") as f1:
	for line in f1:
		ls = line.strip().split()
		w_start.append(ls[2])
		w_end.append(ls[3])
		words.append(ls[4])

with open(phone_ali_file,"r") as f1:
	for line in f1:
		ls = line.strip().split()
		if ls[4] == "SIL":
			sil_count += 1
			print("----------------- : ls[4] : ",ls[4])
		else:
			p_start.append(ls[2])
			p_end.append(ls[3])
			phones.append(ls[4])

print(words)
print(phones)
print(sil_count)

#word_result = []
final_result = []
#final_utt_ = []
ll = 1
with open(data_dir+"/gop_phone","r") as f1:
	for line in f1:
		#print("line : ",line)
		if "[" in line:
			ls = line.replace("[","").replace("]","").split(",")
			score = []
			word_result = []
			final_utt_ = []
			idx = 0
			word_phones = []
			word_index = 0
			word_score = 0
			total_score = 0
			for x in ls:
				if "_E" in str(phones[idx]):
					word_phones.append(str(phones[idx])+":"+str(norm(float(x))))
					score.append(word_phones)
					word_score = word_score + norm(float(x))
					dict_temp = {}
					dict_temp[words[word_index]] = word_phones
					dict_temp["score"] = word_score/len(word_phones)
					dict_temp["norm"] = norm_in_range(word_score/len(word_phones),min_range,max_range)
					total_score = total_score + float(word_score/len(word_phones))
					word_result.append(dict_temp)
					word_phones = []
					word_index += 1
					word_score = 0
				elif "_S" in str(phones[idx]):
					word_phones.append(str(phones[idx])+":"+str(norm(float(x))))
					score.append(word_phones)
					word_score = word_score + norm(float(x))
					dict_temp = {}
					dict_temp[words[word_index]] = word_phones
					dict_temp["score"] = word_score/len(word_phones)
					dict_temp["norm"] = norm_in_range(word_score/len(word_phones),min_range,max_range)
					total_score = total_score + float(word_score/len(word_phones))
					word_result.append(dict_temp)
					word_phones = []
					word_index += 1
					word_score = 0
				else:
					word_score = word_score + norm(float(x))
					word_phones.append(str(phones[idx])+":"+str(norm(float(x))))
				idx += 1 
			#print(len(phones),len(score))
			#print(utt_id,score)
			#print(word_result)
			tmp_1 = {}
			tmp_1["words"] = word_result
			tmp_1["overall_score"] = total_score/len(words)
			final_utt_.append(tmp_1)
			#word_result.append(tmp_1)
			#word_result.append(tmp_)
			final_result.append(final_utt_)
			#print(json.dumps(word_result))
			ll += 1
			#print("--------------------------------------------------------------------")
		else:
			#print("else : ",line)
			utt_id = line.strip()
#print("ll : ",ll)
#print(json.dumps(final_result,ensure_ascii=False))
with open(data_dir+'/result.json', 'w', encoding='utf-8') as f:
	json.dump(final_result, f, ensure_ascii=False)
