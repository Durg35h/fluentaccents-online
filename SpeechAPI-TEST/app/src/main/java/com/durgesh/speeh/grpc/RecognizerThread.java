package com.durgesh.speeh.grpc;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

//import com.durgesh.asr.Model;
//import com.durgesh.asr.Recognizer;


import com.durgesh.speeh.AsrCallbacks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class RecognizerThread extends Thread {
    private int remainingSamples;
    private int timeoutSamples;
    private final static int NO_TIMEOUT = -1;
    private volatile boolean paused = false;
    private volatile boolean reset = false;
    private  volatile  boolean stopRecorder = false;
    int sampleRate;
    float BUFFER_SIZE_SECONDS = 0.2f;
    int bufferSize;
    AudioRecord recorder;
    //    Recognizer recognizer;
    Context context;
    AsrCallbacks asrCallbacks;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public RecognizerThread(int sampleRate, Context context, String ip, int PORT) {
        this.sampleRate = sampleRate;
        this.context = context;
        asrCallbacks = (AsrCallbacks) context;
//        recognizer = new Recognizer().GetInstance(model,sampleRate);
        bufferSize = Math.round(this.sampleRate * BUFFER_SIZE_SECONDS);
        this.recorder = new AudioRecord(6, this.sampleRate, 16, 2, this.bufferSize * 2);
        if (this.recorder.getState() == 0) {
            this.recorder.release();
            Log.e("Durgesh","Faled to init recorder");
        }
        timeoutSamples = NO_TIMEOUT;
    }

    public void setPause(boolean paused) {
        this.paused = paused;
    }

    public void reset() {
        this.reset = true;
    }

    public void stopRecording(){
        this.stopRecorder = true;
    }
    byte [] ShortToByte_ByteBuffer_Method(short [] input)
    {
        byte[] bytes2 = new byte[input.length * 2];
        ByteBuffer.wrap(bytes2).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(input);

        return bytes2;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void run() {
//        Log.e("Durgesh","run");
        recorder.startRecording();
//        String filepath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        String filepath = "/data/data/" + context.getPackageName();
        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filepath+"/record.pcm");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (recorder.getRecordingState() == AudioRecord.RECORDSTATE_STOPPED) {
            recorder.stop();
            Log.e("Durgesh","Failed to start recording. Microphone might be already in use.");
        }
        short[] buffer = new short[bufferSize];
        while (true) {
            int nread = recorder.read(buffer, 0, buffer.length);

//            if (paused) {
//                continue;
//            }
            if (nread < 0){
                throw new RuntimeException("error reading audio buffer");
            }
            try {
                os.write(ShortToByte_ByteBuffer_Method(buffer),
                        0,
                        buffer.length*2);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (timeoutSamples != NO_TIMEOUT) {
                remainingSamples = remainingSamples - nread;
            }
            if (stopRecorder){
                break;
            }
        }
        try {
            os.close();
            Log.e("Durgesh","File saved");
        } catch (IOException e) {
            e.printStackTrace();
        }
        recorder.stop();
        try {
//            rawToWave(new File(filepath + "/record.pcm"),filepath+"/record.wav");
            PCMToWAV(new File(filepath + "/record.pcm"),new File(filepath+"/record.wav"),1,16000,16);
//            Log.e("Durgesh","file converted");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!paused) {
            // If we met timeout signal that speech ended
            if (timeoutSamples != NO_TIMEOUT && remainingSamples <= 0) {
            } else {
//                final String finalResult = recognizer.getFinalResult();
//                asrCallbacks.onFinal(finalResult);
            }
        }

    }

    /**
     * @param input         raw PCM data
     *                      limit of file size for wave file: < 2^(2*4) - 36 bytes (~4GB)
     * @param output        file to encode to in wav format
     * @param channelCount  number of channels: 1 for mono, 2 for stereo, etc.
     * @param sampleRate    sample rate of PCM audio
     * @param bitsPerSample bits per sample, i.e. 16 for PCM16
     * @throws IOException in event of an error between input/output files
     * @see <a href="http://soundfile.sapp.org/doc/WaveFormat/">soundfile.sapp.org/doc/WaveFormat</a>
     */
    static public void PCMToWAV(File input, File output, int channelCount, int sampleRate, int bitsPerSample) throws IOException {
        final int inputSize = (int) input.length();

        try (OutputStream encoded = new FileOutputStream(output)) {
            // WAVE RIFF header
            writeToOutput(encoded, "RIFF"); // chunk id
            writeToOutput(encoded, 36 + inputSize); // chunk size
            writeToOutput(encoded, "WAVE"); // format

            // SUB CHUNK 1 (FORMAT)
            writeToOutput(encoded, "fmt "); // subchunk 1 id
            writeToOutput(encoded, 16); // subchunk 1 size
            writeToOutput(encoded, (short) 1); // audio format (1 = PCM)
            writeToOutput(encoded, (short) channelCount); // number of channelCount
            writeToOutput(encoded, sampleRate); // sample rate
            writeToOutput(encoded, sampleRate * channelCount * bitsPerSample / 8); // byte rate
            writeToOutput(encoded, (short) (channelCount * bitsPerSample / 8)); // block align
            writeToOutput(encoded, (short) bitsPerSample); // bits per sample

            // SUB CHUNK 2 (AUDIO DATA)
            writeToOutput(encoded, "data"); // subchunk 2 id
            writeToOutput(encoded, inputSize); // subchunk 2 size
            copy(new FileInputStream(input), encoded);
        }
    }


    /**
     * Size of buffer used for transfer, by default
     */
    private static final int TRANSFER_BUFFER_SIZE = 10 * 1024;

    /**
     * Writes string in big endian form to an output stream
     *
     * @param output stream
     * @param data   string
     * @throws IOException
     */
    public static void writeToOutput(OutputStream output, String data) throws IOException {
        for (int i = 0; i < data.length(); i++)
            output.write(data.charAt(i));
    }

    public static void writeToOutput(OutputStream output, int data) throws IOException {
        output.write(data >> 0);
        output.write(data >> 8);
        output.write(data >> 16);
        output.write(data >> 24);
    }

    public static void writeToOutput(OutputStream output, short data) throws IOException {
        output.write(data >> 0);
        output.write(data >> 8);
    }

    public static long copy(InputStream source, OutputStream output)
            throws IOException {
        return copy(source, output, TRANSFER_BUFFER_SIZE);
    }

    public static long copy(InputStream source, OutputStream output, int bufferSize) throws IOException {
        long read = 0L;
        byte[] buffer = new byte[bufferSize];
        for (int n; (n = source.read(buffer)) != -1; read += n) {
            output.write(buffer, 0, n);
        }
        return read;
    }

//    class Constants {
//
//        final static public int RECORDER_SAMPLERATE = 16000;
//        final  static public int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
//        final  static public int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
//
//        final static public int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
//        final static public int BytesPerElement = 2; // 2 bytes in 16bit format
//
//
//    }
//    private File rawToWave(final File rawFile, final String filePath) throws IOException {
//
//        File waveFile = new File(filePath);
//
//        byte[] rawData = new byte[(int) rawFile.length()];
//        DataInputStream input = null;
//        try {
//            input = new DataInputStream(new FileInputStream(rawFile));
//            input.read(rawData);
//        } finally {
//            if (input != null) {
//                input.close();
//            }
//        }
//
//        DataOutputStream output = null;
//        try {
//            output = new DataOutputStream(new FileOutputStream(waveFile));
//            // WAVE header
//            // see http://ccrma.stanford.edu/courses/422/projects/WaveFormat/
//            writeString(output, "RIFF"); // chunk id
//            writeInt(output, 36 + rawData.length); // chunk size
//            writeString(output, "WAVE"); // format
//            writeString(output, "fmt "); // subchunk 1 id
//            writeInt(output, 16); // subchunk 1 size
//            writeShort(output, (short) 1); // audio format (1 = PCM)
//            writeShort(output, (short) 1); // number of channels
//            writeInt(output, Constants.RECORDER_SAMPLERATE); // sample rate
//            writeInt(output, Constants.RECORDER_SAMPLERATE * 2); // byte rate
//            writeShort(output, (short) 2); // block align
//            writeShort(output, (short) 16); // bits per sample
//            writeString(output, "data"); // subchunk 2 id
//            writeInt(output, rawData.length); // subchunk 2 size
//            // Audio data (conversion big endian -> little endian)
//            short[] shorts = new short[rawData.length / 2];
//            ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shorts);
//            ByteBuffer bytes = ByteBuffer.allocate(shorts.length * 2);
//            for (short s : shorts) {
//                bytes.putShort(s);
//            }
//            output.write(bytes.array());
//        } finally {
//            if (output != null) {
//                output.close();
//            }
//        }
//
//        return waveFile;
//
//    }
//
//    private void writeInt(final DataOutputStream output, final int value) throws IOException {
//        output.write(value >> 0);
//        output.write(value >> 8);
//        output.write(value >> 16);
//        output.write(value >> 24);
//    }
//
//    private void writeShort(final DataOutputStream output, final short value) throws IOException {
//        output.write(value >> 0);
//        output.write(value >> 8);
//    }
//
//    private void writeString(final DataOutputStream output, final String value) throws IOException {
//        for (int i = 0; i < value.length(); i++) {
//            output.write(value.charAt(i));
//        }
//    }
}
