import uvicorn
from fastapi import FastAPI, UploadFile, File, Request
import time
import os 
import string
import random
import os
import subprocess
import os.path
import json
import shutil
from fluentspeech import Model, KaldiRecognizer
import wave
from jiwer import wer
import sys

app = FastAPI()
model = Model(sys.argv[1])
model_es = Model(sys.argv[2])

@app.api_route("/en/", methods=["GET", "POST", "DELETE"])
async def receive_file(request: Request, file: UploadFile = File(...)):
    text = str(request.query_params).replace("text=","").replace("_"," ")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    filename = f'{dir_path}/uploads/{time.time()}-{file.filename}'
    f = open(f'{filename}', 'wb')
    content = await file.read()
    f.write(content)
    res = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 7))
    dest = "tmp/"+res
    if not os.path.exists(dest):
        os.makedirs(dest)
    op1 = open(dest+"/wav.scp","w")
    op2 = open(dest+"/text","w")
    op3 = open(dest+"/utt2spk","w")
    op4 = open(dest+"/spk2utt","w")
    op1.write("000030012 "+filename+"\n")
    op2.write("000030012 "+text+"\n")
    op3.write("000030012 000030012"+"\n")
    op4.write("000030012 000030012"+"\n")
    op1.close()
    op2.close()
    op3.close()
    op4.close()
    wf = wave.open(filename, "rb")
    gram = "[\""+text+"\",\"[unk]\"]"
    rec = KaldiRecognizer(model, wf.getframerate(),gram)
    asr_res = ""
    while True:
        data = wf.readframes(4000)
        if len(data) == 0:
            break
        if rec.AcceptWaveform(data):
            asr_res = json.loads(rec.Result())
    asr_res_final = json.loads(rec.FinalResult())
    hyp = ""
    if 'text' in asr_res:
        hyp = asr_res['text']
    else:
        hyp = asr_res_final['text']
    error = wer(text, hyp)
    li1 = text.split()
    li2 = hyp.split()
    print("hyp : ",hyp)
    print("error : ",error)
    li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2]
    set_ = set(li_dif)
    all_false = False
    if len(hyp.strip().split(" ")) == len(text.strip().split(" ")):
        hyp_ = hyp.strip().split(" ")
        text_ = text.strip().split(" ")
        match_list = []
        for wrd in range(0,len(hyp_)):
            if text_[wrd] == hyp_[wrd]:
                match_list.append(True)
            else:
                match_list.append(False)
        print("match_list : ",match_list)
    else:
        hyp_ = hyp.strip().split(" ")
        text_ = text.strip().split(" ")
        print("hyp : ",hyp_)
        print("text : ",text_)
    cmd="bash run_english.sh "+dest+" "+str(error)
    subprocess.call(cmd.split(" "))
    res = ""
    if os.path.isfile(dest+"/result_final.json"):
        with open(dest+"/result_final.json") as f1:
            os.remove(filename)
            shutil.rmtree(dest)
            ret2 = json.load(f1)
            return ret2
    else:
        return "something went wrong"

@app.api_route("/es/", methods=["GET", "POST", "DELETE"])
async def receive_file(request: Request, file: UploadFile = File(...)):
    text = str(request.query_params).replace("text=","").replace("_"," ")
    dir_path = os.path.dirname(os.path.realpath(__file__))
    filename = f'{dir_path}/uploads/{time.time()}-{file.filename}'
    f = open(f'{filename}', 'wb')
    content = await file.read()
    f.write(content)
    res = ''.join(random.choices(string.ascii_uppercase + string.digits, k = 7))
    dest = "tmp/"+res
    if not os.path.exists(dest):
        os.makedirs(dest)
    op1 = open(dest+"/wav.scp","w")
    op2 = open(dest+"/text","w")
    op3 = open(dest+"/utt2spk","w")
    op4 = open(dest+"/spk2utt","w")
    op1.write("000030012 "+filename+"\n")
    op2.write("000030012 "+text+"\n")
    op3.write("000030012 000030012"+"\n")
    op4.write("000030012 000030012"+"\n")
    op1.close()
    op2.close()
    op3.close()
    op4.close()
    wf = wave.open(filename, "rb")
    gram = "[\""+text+"\",\"[unk]\"]"
    rec = KaldiRecognizer(model_es, wf.getframerate(),gram)
    asr_res = ""
    while True:
        data = wf.readframes(4000)
        if len(data) == 0:
            break
        if rec.AcceptWaveform(data):
            asr_res = json.loads(rec.Result())
    asr_res_final = json.loads(rec.FinalResult())
    hyp = ""
    if 'text' in asr_res:
        hyp = asr_res['text']
    else:
        hyp = asr_res_final['text']
    error = wer(text, hyp)
    li1 = text.split()
    li2 = hyp.split()
    print("hyp : ",hyp)
    print("error : ",error)
    li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2]
    set_ = set(li_dif)
    all_false = False
    if len(hyp.strip().split(" ")) == len(text.strip().split(" ")):
        hyp_ = hyp.strip().split(" ")
        text_ = text.strip().split(" ")
        match_list = []
        for wrd in range(0,len(hyp_)):
            if text_[wrd] == hyp_[wrd]:
                match_list.append(True)
            else:
                match_list.append(False)
        print("match_list : ",match_list)
    else:
        hyp_ = hyp.strip().split(" ")
        text_ = text.strip().split(" ")
        print("hyp : ",hyp_)
        print("text : ",text_)
    cmd="bash run_spanish.sh "+dest+" "+str(error)
    subprocess.call(cmd.split(" "))
    res = ""
    if os.path.isfile(dest+"/result_final.json"):
        with open(dest+"/result_final.json") as f1:
            os.remove(filename)
            shutil.rmtree(dest)
            ret2 = json.load(f1)
            return ret2
    else:
        return "something went wrong"


if __name__ == "__main__":
    if not os.path.exists("tmp"):
        os.makedirs("tmp")
    uvicorn.run("main:app", port=2700, reload=True,host='0.0.0.0')
