import sys
import json

dest = sys.argv[1]
wer = float(sys.argv[2])
green = "green-75.00-100.99"
yellow = "yellow-70.00-74.99"
red = "red-0-69.99"

range_ = [green,yellow,red]

with open(dest+"/result.json") as f1:
	res = json.load(f1)
	i = 0
	final_res = []
	for x in res:
		if i < 2:
			i += 1
			continue
		for y in x:
			for z in y['words']:
				tmp_dict = {}
				for w in z:
					if w == "score":
						#print(w,z[w])
						score_clr = ""
						for color in range_:
							data_ = color.split("-")
							name_ = data_[0]
							min_ = float(data_[1])
							max_ = float(data_[2])
							if scr_ > min_ and scr_ < max_:
								score_clr = name_
						#print("score_clr : ",score_clr)
						if wer > 0.8 :
							tmp_dict["tag"] = "red"
						else:
							tmp_dict["tag"] = score_clr
					elif w == "norm":
						continue
					else:
						#print(w)
						tmp_dict["word"] = w
						phone_list = []
						for a in z[w]:
							#print("a : ",a)
							val = a.split(":")
							phn_ = val[0]
							scr_ = float(val[1])
							for color in range_:
								data_ = color.split("-")
								if wer > 0.8 :
									name_ = "red"
								else:
									name_ = data_[0]
								min_ = float(data_[1])
								max_ = float(data_[2])
								if scr_ > min_ and scr_ < max_:
									#phone_list.append(phn_+":"+name_)
									phone_list.append(phn_+":"+str(scr_))
							#print(phn_,scr_)
						#print("phone_list : ",phone_list)
						tmp_dict["phone-tags"] = phone_list
				final_res.append(tmp_dict)
		print("----------")
	print(final_res)
	with open(dest+'/result_final.json', 'w', encoding='utf-8') as f:
		json.dump(final_res, f, ensure_ascii=False)
