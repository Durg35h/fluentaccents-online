package com.durgesh.speeh;

public interface AsrCallbacks {
    void onPartial(String res);

    void onResult(String res);

    void onFinal(String res);
}
