FROM debian:bullseye-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        g++ \
        bzip2 \
        unzip \
        make \
        wget \
        git \
        python3 \
        python3-dev \
        python3-websockets \
        python3-setuptools \
        python3-pip \
        python3-wheel \
        zlib1g-dev \
        patch \
        ca-certificates \
        swig \
        cmake \
        xz-utils \
        automake \
        autoconf \
        libtool \
        pkg-config \
        gfortran \
        sox \
        libsndfile1 \
        ffmpeg \
        build-essential \
	make \
	zlib1g-dev \
	libtool \
	pkg-config \
	ca-certificates \
	subversion python2.7 \
	libatlas-base-dev liblapack-dev libblas-dev \
    && rm -rf /var/lib/apt/lists/* 

COPY ./app /app

RUN pip3 install wget grpcio-tools wheel setuptools unidecode cpufeature jiwer fastapi uvicorn numpy scipy python-multipart kaldi_io /app/fluentspeech-0.3.30-py3-none-linux_x86_64.whl

RUN git clone https://github.com/Durgesh92/kaldi_pa_fix /opt/kaldi && \
    cd /opt/kaldi/tools && \
    make -j $(nproc) && \
    cd /opt/kaldi/src && \
    ./configure --shared --mathlib=ATLAS && \
    make depend -j $(nproc) && \
    make -j $(nproc) && \
    find /opt/kaldi  -type f \( -name "*.o" -o -name "*.la" -o -name "*.a" \) -exec rm {} \; && \
    rm -rf /opt/kaldi/.git

EXPOSE 2700
WORKDIR /app/
CMD ["sh", "-c", " python3 ./main.py /app/asr_model_en /app/asr_model_es"]
