package com.durgesh.speeh;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.durgesh.speeh.grpc.RecognizerThread;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements AsrCallbacks, AdapterView.OnItemSelectedListener {
    String[] country = { "Select Model", "RAW", "Optimized (Running on ARM)", "Google"};

    private ToggleButton toggleButton1;
    Button recgnize_button,recgnize_mic_button;
    TextView textView ;
    //    Model asr_model;
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    String model_dir;
    RecognizerThread recognizerThread;
    //    InfiniteStreamRecognize googleStreamRecognize;
//    String grpc_ip = "54.179.41.94";
    String grpc_ip ; //= "140.238.246.197";
    //    String grpc_ip = "192.168.1.141";
//    int port = 5000;
    int port; //= 10086;


    String server_ip = "192.168.1.141";
    String server_port = "2700";
    boolean is_recording = false;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> arrayListSp = new ArrayList<>();
    TextView sentView;
    String pkg_name;
    ProgressDialog dialog;

//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recognizerThread = new RecognizerThread(16000,MainActivity.this,grpc_ip,port);
        textView = findViewById(R.id.result_text);
        sentView = findViewById(R.id.textViewSent);
        pkg_name = this.getPackageName();
        toggleButton1=(ToggleButton)findViewById(R.id.toggleButton);

        toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    // if toggle button is enabled/on
//                    rl.setBackgroundColor(Color.parseColor("#FF63D486"));
                    sentView.setText(getRandomSentence(arrayList));
                    // Make a toast to display toggle button status
                    Toast.makeText(getApplicationContext(),
                            "Toggle is on", Toast.LENGTH_SHORT).show();
                }
                else{
                    // If toggle button is disabled/off
//                    rl.setBackgroundColor(Color.parseColor("#FFD4558C"));
                    sentView.setText(getRandomSentence(arrayListSp));
                    // Make a toast to display toggle button status
                    Toast.makeText(getApplicationContext(),
                            "Toggle is off", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog = new ProgressDialog(this); // this = YourActivity
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle("Loading");
        dialog.setMessage("Loading. Please wait...");
        dialog.setIndeterminate(true);
        dialog.setCanceledOnTouchOutside(false);

//        findViewById(R.id.recognize_file).setOnClickListener(view -> recognizeFile());
        findViewById(R.id.recognize_mic).setOnClickListener(view -> {
            try {
                recognizeMicrophone();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.button_ply).setOnClickListener(view -> {
            try {
                audioPlayer("/data/data/" + this.getPackageName() + "/record.wav");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.INTERNET}, PERMISSIONS_REQUEST_RECORD_AUDIO);
        } else {
//            Log.e("Durgesh","initModel main");
//            initModel();
        }
//        Spinner spin = (Spinner) findViewById(R.id.spinner);
//        spin.setOnItemSelectedListener(this);

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,country);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
//        spin.setAdapter(aa);
//        copyFileOrDir("test.wav");
//
//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(
//                    new InputStreamReader(getAssets().open("fluentworlds-en.txt")));
//
//            // do reading, usually loop until end of file reading
//            String mLine;
//            while ((mLine = reader.readLine()) != null) {
//                //process line
//                if (mLine.trim().split(" ").length > 2 && mLine.trim().split(" ").length < 6){
//                    arrayList.add(mLine.trim());
//                }
//            }
//        } catch (IOException e) {
//            //log the exception
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    //log the exception
//                }
//            }
//        }
        arrayList = readSentncesFromFile("fluentworlds-en.txt");
        arrayListSp = readSentncesFromFile("fluentworlds-es.txt");
//        sentView.setText(arrayList.get(0));
//        int min = 0;
//        int max = arrayList.size();
//        int idx = (int) ((Math.random() * (max - min)) + min);
        sentView.setText(getRandomSentence(arrayList));
//        sentView.setText("i love oranges");
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggleButton1.isChecked()){
                    sentView.setText(getRandomSentence(arrayList));
                }else {
                    sentView.setText(getRandomSentence(arrayListSp));
                }
            }
        });

        String pkg = this.getPackageName();
        findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //                    upload("/data/data/" + pkg + "/record.wav",);
                dialog.show();
                new RetrieveFeedTask().execute(sentView.getText().toString().replace(" ","_"));
            }
        });
    }

    private String getRandomSentence(ArrayList<String> arrayList){
        int min = 0;
        int max = arrayList.size();
        int idx = (int) ((Math.random() * (max - min)) + min);
        return  arrayList.get(idx);
    }

    private ArrayList<String> readSentncesFromFile(String fileName){
        ArrayList<String> arrayList1 = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(fileName)));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                if (mLine.trim().split(" ").length > 2 && mLine.trim().split(" ").length < 6){
                    arrayList1.add(mLine.trim());
                }
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return arrayList1;
    }
//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recognizeMicrophone() throws Exception {
            Toast.makeText(getApplicationContext(),"you can start dictation now",Toast.LENGTH_SHORT).show();
//            Log.e("Durgesh","recognizing with : "+grpc_ip);
            Button m1c =findViewById(R.id.recognize_mic);
            if (!is_recording){
                    recognizerThread = new RecognizerThread(16000,MainActivity.this,grpc_ip,port);
                    recognizerThread.start();
                    is_recording = true;
                    m1c.setText("Stop Recording");
            }else {
                if (recognizerThread != null) {
                    recognizerThread.stopRecording();
                    m1c.setText("Start Recording");
                    is_recording = false;
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Recognizer initialization is a time-consuming and it involves IO,
                // so we execute it in async task
                Log.e("ASR","onRequestPermissionsResult:InitModel");
                copyFileOrDir("test.wav");

//                initModel();
            } else {
                finish();
            }
        }
    }

    public void audioPlayer(String path){
        //set up MediaPlayer
        MediaPlayer mp = new MediaPlayer();

        try {
            mp.setDataSource(path);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void copyFileOrDir(String path) {
        AssetManager assetManager = this.getAssets();
        String assets[] = null;
        try {
            assets = assetManager.list(path);
            if (assets.length == 0) {
                copyFile(path);
            } else {
                String fullPath = "/data/data/" + this.getPackageName() + "/" + path;
                File dir = new File(fullPath);
                if (!dir.exists())
                    dir.mkdir();
                for (int i = 0; i < assets.length; ++i) {
                    copyFileOrDir(path + "/" + assets[i]);
                }
            }
        } catch (IOException ex) {
            Log.e("tag", "I/O Exception", ex);
        }
    }

    private void copyFile(String filename) {
        AssetManager assetManager = this.getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            String newFileName = "/data/data/" + this.getPackageName() + "/" + filename;
            out = new FileOutputStream(newFileName);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void streamingRecognizeFile(String fileName) throws Exception, IOException {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 1){
            grpc_ip = "54.179.41.94";
            port = 5000;
        }else if (position == 2){
            grpc_ip = "140.238.246.197";
            port = 10086;
        }else if (position ==3) {
            port = 7777;

//            googleStreamRecognize = new InfiniteStreamRecognize();
//            Toast.makeText(getApplicationContext(),"Not yet implemented" , Toast.LENGTH_LONG).show();
        }else {
            port = 999;
            Toast.makeText(getApplicationContext(),"Please select Model" , Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPartial(String res) {
        textView.setText(res);
    }

    @Override
    public void onResult(String res) {
        textView.setText(res);
    }

    @Override
    public void onFinal(String res) {
        textView.setText(res);
    }
    private void pause(boolean checked) {
        if (recognizerThread != null) {
            recognizerThread.setPause(checked);
        }
    }

    class RetrieveFeedTask extends AsyncTask<String, Void, String> {

        private Exception exception;

        protected String doInBackground(String... sentence) {
            String urlServer;
            if (toggleButton1.isChecked()){
                urlServer = "http://"+server_ip+":+"+server_port+"/en/?text="+sentence[0];
            }else {
                urlServer = "http://"+server_ip+":"+server_port+"/es/?text="+sentence[0];
            }

            Log.e("Durgesh","url : "+urlServer);
            OkHttpClient client = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();
            MultipartBody.Builder mMultipartBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file","record.wav",RequestBody.create(MediaType.parse("media/type"),new File("/data/data/" + pkg_name + "/record.wav")));


            RequestBody mRequestBody = mMultipartBody.build();
            Request request = new Request.Builder()
                    .url(urlServer).post(mRequestBody)
                    .build();

            Response response = null;
            String responseString = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                responseString = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseString;
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        protected void onPostExecute(String feed) {
            // TODO: check this.exception
            // TODO: do something with the feed
//            feed = feed.replace("\\","");
//            String sq = String.valueOf(Html.fromHtml(parseResult(feed)));
//            Log.e("Durgesh","sq : "+sq);
            textView.setText(Html.fromHtml(parseResult(feed)));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
//            Log.e("Durgesh","feed : "+feed);
            dialog.dismiss();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String parseResult(String result){
        Log.e("Durgesh","result : "+result);
        try {
            JSONArray jsonArray = new JSONArray(result);
            String sent = "";
            String details = "";
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject row = jsonArray.getJSONObject(i);
                String word = row.getString("word");
//                Log.e("Durgesh","word : "+word);
                String color = row.getString("tag");
                if (color.equals("red")){
                    word = "<font color='#FF0000'>"+word+"</font>";
                }else if (color.equals("green")){
                    word = "<font color='#00FF00'>"+word+"</font>";
                }else if (color.equals("yellow")){
                    word = "<font color='#FFFF00'>"+word+"</font>";
                }
//                String score = row.getString("phone-tags");
                JSONArray jsonArray_ = row.getJSONArray("phone-tags");
                details = details + "<br/>" +word+"<br/>";
                for (int j = 0; j < jsonArray_.length(); j++) {
                    Log.e("Durgesh", String.valueOf(jsonArray_.get(j)));
                    String val = String.valueOf(jsonArray_.get(j));
                    String phn = val.split(":")[0].split("_")[0];
                    String scr = val.split(":")[1];
                    details = details + "\t " + phn +" : " + scr+"<br/>";
                }
                details = details + "---------------------\n";
//                Log.e("Durgesh","score : "+score);
                sent = sent + word + " ";
                Log.e("Durgesh","------------");
            }

            Log.e("Durgesh","details : "+details);
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject row = jsonArray.getJSONObject(i);
//                String word = row.getString("word");
//                JSONArray jsonArray_ = new JSONArray(row.getJSONObject("phone-tags"));
//                for (int j = 0; j < jsonArray_.length(); j++) {
//                    Log.e("Durgesh", String.valueOf(jsonArray_.get(j)));
//                }
//            }


//            Log.e("Durgesh","result : "+sent);

            return sent+"<br/>"+details;
//            return details;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}